const fs = require('fs');
const http = require('http');
const socketIo = require('socket.io');

const handler = (req, res) => {
  let path = '';
  switch (req.url) {
    case '/':
      path = '/index.html';
      break;
    case '/index.js':
      path = '/index.js';
      break;
    case '/favicon.ico':
      path = '/favicon.ico';
      break;
    default:
      break;
  }
  fs.readFile(`${__dirname}${path}`, (err, data) => {
    let status;
    let message;

    if (err) {
      status = 500;
      message = `Error loading ${path}`;
    } else {
      status = 200;
      message = data;
    }

    res.writeHead(status);
    res.end(message);
  });
};

const app = http.createServer(handler);
const io = socketIo(app);

app.listen(3001);


io.on('connection', socket => {
  let messageNumber = 0;
  let interval = null;

  interval = setInterval(() => {
    const i = ++messageNumber;

    if (i > 1000) {
      socket.emit('news', { hello: `world - ${i}; LAST MESSAGE` });
      clearInterval(interval);
    } else {
      socket.emit('news', { hello: `world - ${i}` });
    }
  }, 20);

  socket.on('fromClient', data => {
    console.log(data);
  });

});
