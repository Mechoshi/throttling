document.addEventListener("DOMContentLoaded", () => {
  const socket = io('http://localhost:3001');
  const FREQUENCY = 5000; // ms
  const dataContainer = document.getElementById('data');

  const messageHandler = data => {
    console.log(data);
    const pre = document.createElement('pre');
    pre.innerText = JSON.stringify(data, null, 2);
    dataContainer.appendChild(pre);
    socket.emit('fromClient', data);
  };


  const throttle = (handler, frequency, predicate) => {

    // The event data
    let data = null;

    // The beginning of each period
    let start = 0;

    // Timeout's id
    let timeout = null;

    // Return throttled event handler:
    return (...args) => {

      // If called for the first time...
      if (start === 0) {

        // ... set the beginning of the period...
        start = Date.now();

        // ... set the event data
        // to be the last actual...
        data = args;

        // ... and execute the handler:
        handler(...data);

        // On every following call...
      } else {

        // ... use the predicate function
        // if such has been provided:
        if (typeof predicate === 'function') {

          if (predicate(data, args)) {

            // If the predicate returns true
            // then set the data from the
            // current event:
            data = args;

          } else {
            // The data from a previous event
            // will be used.
          }

          // If no predicate...
        } else {

          //... set the data from
          // the current event:
          data = args;
        }

        // The time left before the current
        // period's end:
        const timeTillEndOfPeriod = frequency - (Date.now() - start);

        // Clear the timeout because a new event
        // is fired before the period's end:
        clearTimeout(timeout);

        // Set new timeout handler to be executed
        // after the remaining time till the end
        // of the current period:
        timeout = setTimeout(() => {

          // Get the current timestamp:
          const now = Date.now();

          // Check if the period is over...
          if (now - start >= frequency) {

            // And if it is over,
            // set the beginning
            // of the next period...
            start = now;

            // ... and execute the handler:
            handler(...data);

          } else {

            console.log('Not yet ready');

          }
        }, timeTillEndOfPeriod);
      }

    };

  };

  socket.on('news', throttle(messageHandler, FREQUENCY, (prevData, newData) => {
    return true;
  }));

});